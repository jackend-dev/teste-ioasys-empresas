## Bibliotecas adicionadas ao projeto como dependência:

- RETROFIT/GSON
- implementation 'com.squareup.retrofit2:retrofit:2.9.0'
- implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
** Retrofit ** para trabalhar com as requisições HTTP e o **GSON** para converter os objetos.

- GLIDE
- implementation 'com.github.bumptech.glide:glide:4.12.0'

**Glide** para poder trabalhar com as imagens da api via link, armazenando em cache. (Infelizmente não foi utilizado).

- Coroutine/ViewModel/LiveData
- implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-android:1.5.1-native-mt'
- implementation 'androidx.lifecycle:lifecycle-extensions:2.2.0'
**Coroutine/lifecycle** para trabalhar de forma assíncrona ao implementar o **ViewModel/LiveData**.

## Se eu tivesse tempo...

Infelizmente falhei em cumprir todos os requisitos, tive dificuldades para realizar a integração com a collection oferecida. Mas se eu tivesse mais tempo, faria o possível para finalizar os tópicos validações de entrada e implementaria armazenamento de informações sobre as empresas, utilizando o Room/SQLite e também tentaria implementar Fragments.

## Instruções para executar o projeto:

- Clonar o projeto
- Rodar o app via Android Studio ou device físico.




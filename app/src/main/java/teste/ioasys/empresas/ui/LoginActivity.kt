package teste.ioasys.empresas.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.lifecycle.ViewModelProvider
import teste.ioasys.empresas.utils.LoginPerformed
import teste.ioasys.empresas.databinding.ActivityLoginBinding
import teste.ioasys.empresas.viewModel.LoginActivityViewModel

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginActivityViewModel
    private lateinit var loginPerformed: LoginPerformed
    private lateinit var etEmail: String
    private lateinit var etPassword: String
    lateinit var signInInputsArray: Array<EditText>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        viewModel = ViewModelProvider(this)
            .get(LoginActivityViewModel::class.java)

        loginPerformed = LoginPerformed(this)
        signInInputsArray = arrayOf(binding.etSignInEmail, binding.etSignInPassword)

        binding.btnSignIn.setOnClickListener {
            onSignIn()
        }
    }

    // verifica se os inputs são vazios
    private fun notEmptyCredentials(): Boolean = etEmail.isNotEmpty() && etPassword.isNotEmpty()

    private fun onSignIn() {
        etEmail = binding.etSignInEmail.text.toString().trim()
        etPassword = binding.etSignInPassword.text.toString().trim()

        if (notEmptyCredentials()) {
            binding.layoutProgressBar.visibility = View.VISIBLE

            viewModel.apiCallLogin(etEmail, etPassword)
            viewModel.loginResponse.observe(this, {
                println("response: $it")
                if (it?.id == 0) {
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)

                    loginPerformed.saveAuthToken(it.access_token)
                    binding.layoutProgressBar.visibility = View.GONE
                    binding.msgError.visibility = View.GONE

                } else {
                    binding.layoutProgressBar.visibility = View.GONE
                    binding.msgError.visibility = View.VISIBLE
                    binding.etSignInEmail.error = "Erro"
                    binding.etSignInPassword.error = "Erro"
                    Log.d("TAG", "Erro")

                }
            })

        } else {
            signInInputsArray.forEach { input ->
                if (input.text.toString().trim().isEmpty()) {
                    input.error = "${input.hint} é obrigatório"
                }
            }
        }
    }
}

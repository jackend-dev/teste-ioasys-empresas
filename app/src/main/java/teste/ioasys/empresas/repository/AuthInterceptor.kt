package teste.ioasys.empresas.repository

import android.content.Context
import teste.ioasys.empresas.utils.LoginPerformed
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(context: Context) : Interceptor {
    private val loginPerformed = LoginPerformed(context)

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        // quando o token for salvo adicionar à solicitação
        loginPerformed.getToken()?.let {
            requestBuilder.addHeader("Authorization", "Bearer $it")
        }

        return chain.proceed(requestBuilder.build())
    }
}
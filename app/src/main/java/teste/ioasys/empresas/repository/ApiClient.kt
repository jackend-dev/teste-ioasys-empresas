package teste.ioasys.empresas.repository

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import teste.ioasys.empresas.utils.ApiConst.BASE_URL

object ApiClient {

    val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val apiService = retrofit.create(ApiService::class.java)


}
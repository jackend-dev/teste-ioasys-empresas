package teste.ioasys.empresas.repository

import teste.ioasys.empresas.data.responses.LoginResponse
import teste.ioasys.empresas.data.responses.PostsResponse
import teste.ioasys.empresas.utils.ApiConst
import retrofit2.Call
import retrofit2.http.*

import retrofit2.http.POST

import retrofit2.http.FormUrlEncoded
import teste.ioasys.empresas.data.model.Enterprise
import teste.ioasys.empresas.utils.ApiConst.BASE_URL
import teste.ioasys.empresas.utils.ApiConst.ENTERPRISE_TYPES
import teste.ioasys.empresas.utils.ApiConst.LOGIN_URL
import teste.ioasys.empresas.utils.ApiConst.NAME


interface ApiService {

    @FormUrlEncoded
    @POST(LOGIN_URL)
    fun login(
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<LoginResponse>

    @GET("enterprises")
    fun listEnterprises(
        @Query(ENTERPRISE_TYPES) enterpriseType: Int = 1,
        @Query(NAME) name: String
    ): Call<PageList>

    @GET(ApiConst.POSTS_URL)
    fun fetchPosts(): Call<PostsResponse>

}

class PageList(val item: List<Enterprise>)
package teste.ioasys.empresas.utils

import android.content.Context
import android.content.SharedPreferences
import teste.ioasys.empresas.R


/*
 buscar os dados do sharedPreferences para salvar a cada login realizado
 */
class LoginPerformed (context: Context) {
    private var sharedPreferences: SharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name),
        Context.MODE_PRIVATE)

    companion object {
        const val USER_TOKEN = "access_token"
    }

    // salvar o token
    fun saveAuthToken(token: String) {
        val editor = sharedPreferences.edit()
        editor.putString(USER_TOKEN, token)
        editor.apply()
    }

    // buscar o token
    fun getToken(): String? {
        return sharedPreferences.getString(USER_TOKEN, null)
    }
}
package teste.ioasys.empresas.utils

object ApiConst {

    // Endpoints
    const val BASE_URL = "https://empresas.ioasys.com.br/api/v1/"
    const val LOGIN_URL = "users/auth/sign_in"
    const val ENTERPRISE_TYPES = "enterprise_types"
    const val NAME = "name"
    const val POSTS_URL = "dataPosts"

}
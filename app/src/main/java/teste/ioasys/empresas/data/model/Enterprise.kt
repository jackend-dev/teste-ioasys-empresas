package teste.ioasys.empresas.data.model

data class Enterprise(
    val id: Int,
    val name: String
)
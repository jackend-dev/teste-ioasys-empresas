package teste.ioasys.empresas.data.responses

import com.google.gson.annotations.SerializedName
import teste.ioasys.empresas.data.model.User

data class LoginResponse(
    @SerializedName("id")
    var id: Int,

    @SerializedName("access_token")
    var access_token: String,

    val user: User
)
package teste.ioasys.empresas.data.model

data class DataPost (
    var id: Int,
    var content: String
)
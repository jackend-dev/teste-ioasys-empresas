package teste.ioasys.empresas.data.responses

import com.google.gson.annotations.SerializedName
import teste.ioasys.empresas.data.model.DataPost

data class PostsResponse (
    @SerializedName("status_code")
    var status: Int,

    @SerializedName("message")
    var message: String,

    @SerializedName("dataPosts")
    var dataPosts: List<DataPost>
)
package teste.ioasys.empresas.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import teste.ioasys.empresas.data.responses.LoginResponse
import teste.ioasys.empresas.repository.ApiClient

class LoginActivityViewModel : ViewModel() {
    private val _loginLiveData: MutableLiveData<LoginResponse> = MutableLiveData()

    val loginResponse: LiveData<LoginResponse>
        get() = _loginLiveData

    fun apiCallLogin(email: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val call = ApiClient.apiService.login(email, password)
            call.enqueue(object : Callback<LoginResponse> {
                override fun onResponse(
                    call: Call<LoginResponse>,
                    response: Response<LoginResponse>
                ) {
                    _loginLiveData.postValue(response.body())
                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    Log.d("TAG", "Erro")
                }
            })
        }
    }

}